//
//  NSString+GLSHA256.h
//  GLive
//
//  Created by Gordon Su on 17/4/10.
//  Copyright © 2017年 tencent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (GMAccountSecure)

NSData * hmacForKeyAndData(NSString *key, NSString *data);

//- (NSString *)SHA256;

- (NSString *)sha256;

- (NSString *)sha256AndUpperCase;

- (NSString *)base64EncodedString:(NSString *)string;

- (NSString *)base64DecodedString:(NSString *)string;

- (NSString *)safeUrlBase64Encode:(NSString *)str;

- (NSString *)safeUrlBase64Decode:(NSString *)safeUrlbase64Str;

+ (NSString *)hmac:(NSString *)plaintext withKey:(NSString *)key;

+ (NSString *)hmacSHA256AndSafeUrlBase64EncodeWithKey:(NSString *)key value:(NSString *)value;


@end

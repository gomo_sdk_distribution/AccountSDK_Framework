//
//  GLResponse.h
//  GLive
//
//  Created by Gordon Su on 17/4/11.
//  Copyright © 2017年 tencent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GMAccountError.h"

typedef NS_ENUM(NSInteger , GWSHttpStatus) {
    GWSHttpStatusFail = -1,
    GWSHttpStatusSuccess = 0,
    GWSHttpStatusInvalidSign = -100,//无效的签名
            GWSH
};

@interface GMAccountHTTPResponse : NSObject
@property (nonatomic) GWSHttpStatus status;


/**
 http 的返回码
 */
@property (nonatomic) NSInteger statusCode;
@property (nonatomic, strong) GMAccountError *error;
@property (nonatomic, copy) NSDictionary *bodyData;
@property (nonatomic, copy) NSString *bodyString;
@property (nonatomic, strong) NSURLResponse *response;


/**
 资源路径
 */
@property (nonatomic, copy) NSString *path;

@property (nonatomic, copy) NSDictionary *httpHeader;


- (BOOL)isSuccess;

- (NSString *)errorLog;

- (NSString *)errorTips;


@end

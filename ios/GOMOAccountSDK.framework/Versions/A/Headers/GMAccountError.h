//
//  GMAccountError.h
//  GLive
//
//  Created by Gordon Su on 17/4/11.
//  Copyright © 2017年 tencent. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, GLLocalErrorCode) {
    GLLocalErrorCodeApiInvalidParam = 1000000,
};

@interface GMAccountError : NSObject

/* 
 服务器定义字段 
 */
@property (nonatomic, copy) NSString *errorCode;
@property (nonatomic, copy) NSString *errorMsg;

/**
 服务器error_result中包含success

 @return 是否的确发生错误
 */
- (BOOL)isNoError;

/**
 利用NSError创建一个GLError

 @param error NSError的元数据
 @return 构建的新对象
 */
- (instancetype)initWithError:(NSError *)error;

/**
 构建本地Api请求参数检查不合法的错误

 @param apiDomain api域名
 @param userInfo 自定义信息
 @return 错误对象
 */
+ (instancetype)errorWithApiDomain:(NSString *)apiDomain
                          userInfo:(NSDictionary *)userInfo;



/**
 内部使用的error的userInfo字段

 @return 内部使用的error的userInfo字段
 */
- (NSDictionary *)userInfo;


/**
 本地化的提示

 @return 本地化的提示
 */
- (NSString *)localizedErrorMessage;

- (NSError *)error;
@end

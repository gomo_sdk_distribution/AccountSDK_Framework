//
//  GMAccountURL.h
//  GLive
//
//  Created by Gordon Su on 17/4/11.
//  Copyright © 2017年 tencent. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GMAccountHTTPRequest;

@interface GMAccountURL : NSObject

@property (nonatomic, copy, readonly) NSString *base;

/**
 * 主要用于签名
 */
@property (nonatomic, copy, readonly) NSString *domain;

- (instancetype)initWithUrl:(NSString *)url domain:(NSString *)domain;

@property (nonatomic, weak) GMAccountHTTPRequest *request;

/**
 * 生成请求体参数
 */
- (NSDictionary *)buildPayload;

/**
 * 生成查询参数集合
 */
- (NSDictionary *)buildQueries;

/**
 * 生成查询参数字符串 ie:a=a&b=b
 */
- (NSString *)buildQueryString:(BOOL) encodeUTF8;

/**
 * 生成完整地址，域名+资源路径+查询参数
 */
- (NSString *)buildFullUrlString;


/**
 增加查询参数

 @param key <#key description#>
 @param value <#value description#>
 */
- (void)addQueriesWithKey:(NSString *)key value:(id)value;

- (void)addQueriesWithDic:(NSDictionary *)dic;

//- (void)parseQueryStr:(NSString *)query;

@end

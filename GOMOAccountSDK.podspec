Pod::Spec.new do |s|
  s.name = "GOMOAccountSDK"
  s.version = "0.1.3"
  s.summary = "A short description of GOMOAccountSDK."
  s.license = {"type"=>"MIT", "file"=>"LICENSE"}
  s.authors = {"dengnengwei"=>"dengnengwei@gomo.com"}
  s.homepage = "https://gitlab.com/gomo_sdk/GOMOAccountSDK"
  s.description = "TODO: Add long description of the pod here."
  s.source = { :path => '.' }

  s.ios.deployment_target    = '8.0'
  s.ios.vendored_framework   = 'ios/GOMOAccountSDK.framework'
  s.ios.public_header_files = 'ios/GOMOAccountSDK.framework/Headers/*.h'
end

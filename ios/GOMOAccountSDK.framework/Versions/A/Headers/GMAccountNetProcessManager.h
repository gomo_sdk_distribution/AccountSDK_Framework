//
// Created by zhangjiemin on 2018/2/28.
// Copyright (c) 2018 gomo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GMAccountHTTPResponse;

typedef void (^Net403StateProcessBlcok)(void);

typedef void(^NetRequestTimeMonitorBlcok)(GMAccountHTTPResponse *response, NSTimeInterval startRequestTime);

typedef void (^HandmultiLanguageResourceFromNetBlock)(NSString* url, BOOL isNeedUpdate,long long updateTime);
typedef  void (^FollowingHandleBlock)(GMAccountHTTPResponse *response, NSString* followUserID, NSString* action);

typedef NSString *(^GetUserIDBlock)();  //获取UserID的全局Block
@interface GMAccountNetProcessManager : NSObject

//@property (nonatomic, copy) NSString * currentUserID;

/// 普通直播间主播 或者 联播房间大主播 交易信息
@property (nonatomic, copy) GetUserIDBlock getUserIDBlock;

@property (nonatomic, copy) NSString * transformationTokenInfo;

/// 联播房间小主播交易信息
@property (nonatomic, strong) NSMutableDictionary *multiLivingAnchorTranAuthTokenDict;


/**
 * 处理全局的403网络请求
 */
@property (nonatomic, copy) Net403StateProcessBlcok net403StateProcessBlcok;
@property (nonatomic, copy) Net403StateProcessBlcok tokenInvalideProcessBlock;
@property (nonatomic, copy) NetRequestTimeMonitorBlcok netRequestTimeMonitorBlcok;

+ (GMAccountNetProcessManager *)sharedManager;
@end

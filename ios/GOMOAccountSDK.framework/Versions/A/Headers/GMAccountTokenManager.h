//
//  GMAccountTokenManager.h
//  GLive
//
//  Created by Gordon Su on 17/4/12.
//  Copyright © 2017年 tencent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GMAccountTokenInfo.h"

@interface GMAccountTokenManager : NSObject

@property (nonatomic, strong, readonly) GMAccountTokenInfo *tokenInfo;

+ (GMAccountTokenManager *)sharedManager;

- (void)updateTokenInfo:(GMAccountTokenInfo *)token;


/**
 清除token信息，包括本地的
 */
- (void)cleanTokenInfo;


/**
 判断token是否过期去刷新token,应用启动时应该显式的调用
 */
- (void)refreshTokenIfNeed;
//
///**
// 强制刷新token
// */
//- (void)refreshToken;

- (BOOL)needRefreshToken;

@end

//
//  Constants.h
//  LVBIMDemo
//
//  Created by realingzhou on 16/8/22.
//  Copyright © 2016年 tencent. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


extern NSString * kAccountCenterDomain;
extern NSString * KHttpBasicAuthorizationUserName;
extern NSString * kHttpBasicAuthorizationUserValue;
extern NSString * kAccountCenterApiXSignatureSignatureKey;
extern NSString * kEnvtype;


@interface GMAccountConfigs:NSObject

+ (void)setConfigIsTest:(BOOL) isTest
              client_id:(NSString *) client_id
          client_secret:(NSString *) client_secret
          signature_key:(NSString *) signature_key
         des_secret_key:(NSString *) des_secret_key;
    
+(void) setDebugLog:(BOOL) debugLog;
@end

//
//  GLApiHelper.h
//  GLive
//
//  Created by Gordon Su on 17/4/7.
//  Copyright © 2017年 tencent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GMAccountHTTPRequest.h"
#import "GMAccountHTTPResponse.h"

FOUNDATION_EXTERN NSString const *RESPONSE;

typedef void (^GMAccountHTTPResponseSucceedHandler)(GMAccountHTTPResponse *response);

@interface GMAccountHttpHelper : NSObject

- (void)startAsyncWithRequest:(GMAccountHTTPRequest *)request finish:(GMAccountHTTPResponseSucceedHandler)finish;

@end

//
//  GWSRequest.h
//  GLive
//
//  Created by Gordon Su on 17/4/11.
//  Copyright © 2017年 tencent. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GMAccountURL;

typedef NS_ENUM (NSInteger, GWSHttpAuthenticationType) {
    GWSHttpAuthenticationTypeNone = 0,
    GWSHttpAuthenticationTypeBasic,//HTTP BASIC认证
    GWSHttpAuthenticationTypeToken,//账户系统的Access Token认证
    GWSHttpAuthenticationTypeTransactionToken,//交易token
    GWSHttpAuthenticationTypeTransactionAccessToken//交易access token
};

typedef NS_ENUM (NSInteger, GWSHttpMethod) {
    GWSHttpMethodGET = 0,
//    GWSHttpMethodGETLastModified,//Conditional Get
//    GWSHttpMethodGETETag,//Conditional Get
    GWSHttpMethodPOST,
    GWSHttpMethodPUT,
    GWSHttpMethodDELETE,
    GWSHttpMethodUPDATE,
};

@interface GMAccountHTTPRequest : NSObject

@property (nonatomic, readonly) GMAccountURL *URL;

@property (nonatomic,copy) NSString *originQueryString; //最原始的请求URL,在搜索请求中，非英文，需要UTF8转码，而使用URL生成签名时，又不需要对非英文转码，所以需要保留最原始的URL请求字符串

@property (nonatomic, assign) GWSHttpAuthenticationType authenticationType;

@property (nonatomic, assign) GWSHttpMethod method;

@property (nonatomic, copy) NSDictionary *queries;//参数

@property (nonatomic, strong, readonly) NSMutableDictionary *additionalQueryDictionary;//增加参数

@property (nonatomic, copy) NSString *XSignatureKey;

@property (nonatomic) BOOL needDeviceForGet;

@property (nonatomic) BOOL needCrypto;

@property (nonatomic) BOOL isForShortVideo;

/**
 * 短视频位置信息，专门为短视频提供Phead数据的
 */
@property (nonatomic, copy) NSString *locationForVideo;

/**
 用于(glive-api)
 */
@property (nonatomic) BOOL needXUserIdHeader;

- (instancetype)initWithBaseUrl:(NSString *)url domain:(NSString *)domain;

/**
 参数会加到additionalQueryDictionary
 */
- (void)setQueryValue:(id)value forKey:(NSString *)key;

/**（请求方法）用于X-Signature签名 **/
- (NSString *)methodName;
/*（请求路径） 用于X-Signature签名 **/
- (NSString *)requestUri;
/*（查询字符串）用于X-Signature签名 **/
- (NSString *)queryString;
/*（请求体）用于X-Signature签名 **/
- (NSString *)payload;

@end

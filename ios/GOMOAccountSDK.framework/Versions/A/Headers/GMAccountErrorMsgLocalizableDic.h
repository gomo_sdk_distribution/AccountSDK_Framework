//
//  GMAccountErrorMsgLocalizableDic.h
//  GLive
//
//  Created by Gordon Su on 2017/8/2.
//  Copyright © 2017年 3g.cn. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const JSON_EXCEPTION;   // json 解析异常
extern NSString *const NETWORK_ERROR;  //网络异常

extern NSString *const INVALID_PHONENUMBER;  //手机号码无效
extern NSString *const INCORRECT_PASSWORD;  //密码不正确
extern NSString *const INCORRECT_OLD_PASSWORD;  //旧密码不正确
extern NSString *const NEW_PASSWORD_SAME_TO_OLD_PASSWORD;  //新密码和旧密码相同
extern NSString *const SEND_SMS_FAIL;  //发送短信验证码失败
extern NSString *const VERIFICATION_CODE_EXPIRED;  //验证码已过期
extern NSString *const INVALID_VERIFICATION_CODE;  //验证码无效
extern NSString *const USER_REGISTERED;  //手机号已被注册
extern NSString *const VERIFICATION_CODE_AUTH_NOT_PRESENT;  //没有相应的验证码授权（或可能验证码授权已过期）
extern NSString *const USER_NOT_FOUND;  //指定的已存在的用户(手机注册用户或第三方登陆注册用户)不存在
extern NSString *const USER_HAS_BOUND;  //指定的已存在的注册用户已经绑定了其它账号
extern NSString *const ACCESS_TOKEN_ERROR;  //token过期（需要用户重新登陆）
extern NSString *const REACH_LIMIT;  //用户已达到对应的经验值类型的赠送上限
extern NSString *const NOT_BINDING;  //指定的解绑账号并没有发生过绑定
extern NSString *const NOT_SUPPORTED;  //该注册方式不支持绑定操作

extern NSString *const INVALID_PARAM;  //无效请求参数
extern NSString *const INVALID_LIVE_ROOM;  //直播房间Id无效
extern NSString *const USER_FROZEN;  //用户账号被冻结

extern NSString *const USER_CANCELLED;  //用户取消支付
extern NSString *const INVALID_AUTH_TOKEN;  //X-Auth-Token值无效，客户端需要重新获取token值
extern NSString *const AUTH_TOKEN_FAIL;  //服务器内部认证Token失败，此种情况客户端可进行重试
extern NSString *const PARTNER_SERVICE_UNAVAILABLE;  //服务器请求支付合作方失败

extern NSString *const BALANCE_NO_ENOUGH;  //用户虚拟币余额不足
extern NSString *const DUPLICATE_TRAN;  //客户端对同一个交易订单（交易订单号由客户端生成）重复请求
extern NSString *const INVALID_VCOIN;  //无效的虚拟币

extern NSString *const PACKET_SLOT_SIZE_LIMIT;  //包裹中的道具槽已经超出限制
extern NSString *const PACKET_SLOT_ITEM_SIZE_LIMIT;  //当前物品槽中的道具个数已经超出限制
 
 

@interface GMAccountErrorMsgLocalizableDic : NSObject

+ (NSString *)handleCode:(NSString *)key;

@end

//
//  GMAccountDeviceInfo.h
//  GLive
//
//  Created by Gordon Su on 17/4/10.
//  Copyright © 2017年 tencent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GMAccountDeviceInfo : NSObject

+ (NSDictionary *)device;
+ (NSDictionary *)deviceWithDid:(NSString *)did;

+ (NSDictionary *)deviceForNetworkMonitor;

+ (NSString *)getBundleId;

+ (NSString *)getDeviceUUID;
+ (NSString *)UDIDString;
+ (NSString *)advertiseIDString;

//获取对应的国家电话区码
+ (NSString *)getCurrentMoblileCountryCode;

+ (NSString *)getDeviceCountryName;

+ (NSString *)getDeviceLangName;
+ (NSString *)getAppVersion;

+ (NSString *)getAppBuildVersion;

+ (NSString *)getiOSVersion;

+ (NSString *)getCPUType;

+ (NSString *)getPackageName;

+ (NSString *)getIPAddress;

+ (NSArray *)getDNSAddresses;

+ (NSString*)getIPAddressByHostName:(NSString*)strHostName;

+ (BOOL)getIsIpad;

@end
